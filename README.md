Coin Fury
=========

Introduction
--------------
Randomized 2D platform jumping runner where by jump, double jumping, and jumping off walls the player can survive and collect coins to increase their score

Platform
--------------
Android and iOS

Controls
--------------
Tap to Jump
Tap again to Double Jump (jump in the air)
Tap to jump off a wall and reverse direction

Additional Mechanics
--------------
Player is automatically always moving towards the right at level start

Scenes
--------------
Title Screen
Main Game
Score Board (game over)

Storyboard
--------------
*Title Screen*
Coin Fury logo pops in and ‘bounces’ like jelly; Charming twinkling sound
Checkered repeating pattern in grey depicting blocks and coins moves up and to the left in background

*On Tap*
White ‘film’ cut transition to gameplay scene; same twinkling sound
Player is visible in idle; “Tap to Start”

*Game Start*
Player begins running to the right

Environmental Hazards
--------------
Moving Walls
Spike Wall
Spike floor
Spike ceiling
Bashing pillars
Shooting Blades (shoot out from wall)
Moving Enemies (along path)

Graphics
--------------
Friendly, colourful graphics that will appeal to most people

Music
--------------
| Scene         | Theme       | Link
| ------------- | ----------- | ----------- |
| Title         | Loungy and chip tune | http://www.youtube.com/watch?v=XmVvoeHjPo4
| Gameplay      | Upbeat, colorful | http://youtu.be/XmVvoeHjPo4?t=46s
| Scoreboard    | Simply melody for score board after death | http://www.youtube.com/watch?v=1NsAl8GsEtA

*Note: Scoreboard music is a bit complex for the purposes of a scoreboard, but this composition is the perfect theme for the entire game

Sound Effects
--------------
https://www.youtube.com/watch?v=oPcg2XdUbis
Menu Focus 5:44
Menu OK 5:45
Coin Pickup: 5:02
Jump 2:05
Toast 10:29  (use for opening High Score window)
Chomp 15:28

Effects
--------------
Player leaves a trail of smoke behind them (particle effect)

Moments Of Awesome
--------------
See similar npc as player in a little row boat